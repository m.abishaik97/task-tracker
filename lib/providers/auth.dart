import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../constants/endpoint.dart';
import '../exceptions/http_exception.dart';

class Auth with ChangeNotifier {
  String _userId;

  Future<void> signup(String username, String password) {
    return _authenticate(username, password, SIGNUP_URL);
  }

  Future<void> signin(String username, String password) {
    return _authenticate(username, password, SIGNIN_URL);
  }

  Future<void> _authenticate(
      String username, String password, String urlSegment) async {
    final url = urlSegment;
    try {
      final response = await http.post(url,
          body: json.encode({'username': username, 'password': password}));
      final responseData = json.decode(response.body);
      print(responseData);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      _userId = responseData['userId'];
      print(_userId);
    } catch (error) {
      throw error;
    }
  }
}
